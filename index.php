<?php require_once 'code.php'; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PHP SC S02</title>
</head>
<body>
	<h2>Repetition Control Structures</h2>

	<h3>While Loop</h3>
	<p><?php whileLoop(); ?></p>

	<h3>Do-While Loop</h3>
	<p><?php doWhileLoop(); ?></p>

	<h3>For Loop</h3>
	<p><?php forLoop(); ?></p>

	<h2>Array Manipulation</h2>

	<h3>Simple Array</h3>
	<ul>
		<?php foreach ($grades as $grade): ?>
			<li><?php echo $grade; ?></li>
		<?php endforeach ?>
	</ul>

	<h3>Associative Array</h3>
	<ul>
		<?php foreach ($gradePeriods as $period => $grade): ?>
			<!-- Shortcut for php echo -->
			<li>Grade in <?= $period; ?> is <?= $grade; ?></li>
		<?php endforeach ?>
	</ul>

	<h3>Multi-Dimensional Array</h3>
	<ul>
		<?php 
			foreach ($heroes as $team) {
				foreach ($team as $member) {
		?>
			<li><?= $member; ?></li>
		<?php 			
				}
			}
		?>
	</ul>

	<!-- Using for -->
	<ul>
		<?php 
			for ($i=0; $i < count($heroes) ; $i++) { 
			 	for ($j=0; $j < count($heroes[$i]) ; $j++) { 	
		?>
			<li><?= $heroes[$i][$j]; ?></li>
		<?php 			
				}
			}
		?>
	</ul>

	<h3>Multi-Dimensional Associative Array</h3>
	<ul>
		<?php 
			foreach ($powers as $label => $powerGroup) {
				foreach ($powerGroup as $power) {
		?>
			<li><?= "$label: $power"; ?></li>
		<?php 			
				}
			}
		?>
	</ul>

	<h2>Array Functions</h2>

	<h3>Array Push</h3>
	<?php array_push($computerBrands, 'Apple'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Array Unshift</h3>
	<?php array_unshift($computerBrands, 'Dell'); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Array Remove</h3>
	<?php array_pop($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Array Shift</h3>
	<?php array_shift($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Sort / Reverse</h3>
	<?php sort($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<?php rsort($computerBrands); ?>
	<pre><?php print_r($computerBrands); ?></pre>

	<h3>Count</h3>
	<?= count($computerBrands); ?>

	<h3>In Array</h3>	
	<?= searchBrand('Asus', $computerBrands); ?>
</body>
</html>