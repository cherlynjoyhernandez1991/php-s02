<?php

/*
[SECTION] Repetition Control Structures

A while loop takes a single condition. For as long as the condition is true, the code inside the block will run.
*/

function whileLoop() {
	$count = 0;

	while ($count <= 5) {
		echo $count . '<br>';
		$count++;
	}
}

/*
A do while loop the block of code executed once and then condition is evaluated.
*/

function doWhileLoop() {
	$count = 20;

	do {
		echo $count . '<br>';
		$count--;
	} while ($count > 0);
}


/*
The for loop is used when you know in advance how many times the script should run.
	- init counter: Initialize the loop counter value
	- condition counter: Evaluated for each loop iteration. If it evaluates to TRUE, the loop continues. If it evaluates to FALSE, the loop ends.
	- increment counter: Increases the loop counter value
*/
function forLoop() {
	for ($count=0; $count <= 10; $count++) { 
		echo $count . '<br>';
	}
}

// [SECTION] Array Manipulation

$studentNumbers = array('1923', '1924', '1925', '1926');

// Simple array
// $studentNumbers = ['1923', '1924', '1925', '1926'];
$grades = array(98.5, 94.3, 89.2, 90.1);
// Associative Arrays
$gradePeriods = [
	'firstGrading' => 98.5, 
	'secondGrading' => 94.3,
	'thirdGrading' => 89.2,
	'fourthGrading' => 90.1
];
// Multi-Dimensional Arrays
$heroes = [
	['Iron Man', 'Thor', 'Hulk'],
	['Wolverine', 'Cyclops', 'Jean Grey'],
	['Batman', 'Superman', 'Woder Woman']
];
// Multi-Dimensional Associative Arrays
$powers = [
	'regular' => ['Repulsor Blast', 'Rocket Punch'],
	'signature' => ['Unibeam']
];

$computerBrands = ['Acer', 'Asus'];

//Searching Array
function searchBrand($brand, $brandsArr) {
	if (in_array($brand, $brandsArr)) {
		return "$brand is in the array.";
	} else {
		return "$brand is NOT in the array.";
	}
}